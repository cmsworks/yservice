<?php

use app\widgets\Alert;
use app\modules\admin\Module;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;

/* @var $this \yii\web\View */
/* @var $content string */

/** @var \yii\web\Controller $context */
$context = $this->context;

if (isset($this->params['breadcrumbs'])) {
    $panelBreadcrumbs = ['label' => Module::t('module', 'ADMIN'), 'url' => ['/admin/default/index']];
    $breadcrumbs = $this->params['breadcrumbs'];
} else {
    $breadcrumbs = [];
}
?>
<?php $this->beginContent('@app/views/layouts/layout.php'); ?>

<?php
NavBar::begin([
    'brandLabel' => Yii::t('app', 'NAV_ADMIN'),
    'brandUrl' => ['/admin/default/index'],
    'options' => [
        'class' => 'navbar-inverse navbar-fixed-top',
    ],
]);
echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-right'],
    'activateParents' => true,
    'items' => array_filter([
        ['label' => Yii::t('app', 'NAV_LOGOUT'), 'url' => ['/user/default/logout'], 'linkOptions' => ['data-method' => 'post']]
    ]),
]);
NavBar::end();
?>

<div class="container">
    <div class="row">
        <div class="col-md-3">
            <?php
            echo Nav::widget([
                'options' => ['class' => 'nav nav-pills nav-stacked'],
                'activateParents' => true,
                'items' => array_filter([
                    ['label' => Yii::t('app', 'NAV_HOME'), 'url' => ['/admin/default/index'], 'active' => $context->module->id == 'admin'],
                    ['label' => Yii::t('app', 'NAV_ADMIN_USERS'), 'url' => ['/admin/user/default/index'], 'active' => $context->module->id == 'user'],
                ]),
            ]);
            ?>
        </div>
        <div class="col-md-9">
            <?= Breadcrumbs::widget([
                'homeLink' => isset($panelBreadcrumbs) ? $panelBreadcrumbs : null,
                'links' => isset($breadcrumbs) ? $breadcrumbs : [],
            ]) ?>
            <?= Alert::widget() ?>
            <?= $content ?>
        </div>
    </div>
</div>

<?php $this->endContent(); ?>