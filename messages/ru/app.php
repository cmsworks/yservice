<?php

return [
    'NAV_HOME' => 'Главная',
    'NAV_CONTACT' => 'Контакты',
    'NAV_SIGNUP' => 'Регистрация',
    'NAV_LOGIN' => 'Вход',
    'NAV_PROFILE' => 'Профиль',
    'NAV_LOGOUT' => 'Выход',
    'NAV_ADMIN' => 'Администрирование',
    'NAV_ADMIN_SHOP' => 'Магазин',
    'NAV_ADMIN_USERS' => 'Пользователи',
];