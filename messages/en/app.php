<?php

return [
    'NAV_HOME' => 'Home',
    'NAV_CONTACT' => 'Contact',
    'NAV_SIGNUP' => 'Signup',
    'NAV_LOGIN' => 'Login',
    'NAV_PROFILE' => 'Profile',
    'NAV_LOGOUT' => 'Logout',
    'NAV_ADMIN' => 'Admin',
    'NAV_ADMIN_USERS' => 'Users',
];